# Blog app
This is a web application built with Python and ready to be deployed on your server.

You can take a look at a working example [here](https://blog.mstefan99.com/).

#### Main features
* Browsing posts on the website
* User registration
* Adding and removing any posts from favorites
* Browsing saved posts
* Password recovery
* Interactive and validated forms
* User account and settings

#### Features in development
* Search by tags and post content
* Email newsletter
* Admin console
